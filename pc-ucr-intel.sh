#!/bin/bash
# Using ONLY internal BLAS ----- START
export USE_INTERNALBLAS=No
# Using ONLY internal BLAS ----- END
# Using openmpi 1.4.3 (which worked on my laptop)
export ARMCI_NETWORK=SOCKETS
export USE_SCALAPACK=yes
export SCALAPACK_SIZE=4
export SCALAPACK="-L$MKLROOT/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm"
export USE_LAPACK=yes
export LAPACK="-L/opt/intel/mkl/lib/intel64 -lmkl_lapack95_ilp64 -lmkl_blas95_ilp64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential  -lpthread -lm"

### Ravindra : Provide the location where the openmpi is installed
export NWCHEM_TOP=$PWD
export MPI_LOC=/opt/intel/impi/2019.3.199/intel64

export MKL=/opt/intel/mkl/lib/intel64
export HAS_BLAS=yes
export BLAS_SIZE=8
export BLASOPT="-L/opt/intel/mkl/lib/intel64 -lmkl_lapack95_ilp64 -lmkl_blas95_ilp64 -lmkl_intel_ilp64 -lmkl_intel_thread  -lmkl_core -liomp5 -lpthread -lm -ldl"

export LARGE_FILES=TRUE
export NWCHEM_TARGET=LINUX64

export PYTHONHOME=/opt/intel/intelpython3/bin
export PYTHONVERSION=3.6
export USE_PYTHON64=y
export ENABLE_COMPONENT=yes
export USE_MPI=y

#export MPI_LIB=$MPI_LOC/lib64
export MPI_LIB=$MPI_LOC/lib
export MPI_INCLUDE=$MPI_LOC/include
#export LIBMPI="--enable-new-dtags -L/usr/mpi/intel/openmpi-1.4.3/lib64 -lmpi -lmpigf -lmpigi -lpthread -lrt"
export FC=/opt/intel/impi/2019.3.199/intel64/bin/mpiifort
export CC=/opt/intel/impi/2019.3.199/intel64/bin/mpiicc
export MPI_F90=/opt/intel/impi/2019.3.199/intel64/bin/mpiifort
export MPI_CC=/opt/intel/impi/2019.3.199/intel64/bin/mpiicc
export MPI_CXX=/opt/intel/impi/2019.3.199/intel64/bin/mpicxx

export LIBMPI="-L/opt/intel/impi/2019.3.199/intel64/lib -L/opt/intel/impi/2019.3.199/intel64/lib/release -lmpi -lmkl_lapack95_ilp64 -lmkl_blas95_ilp64 -lmkl_scalapack_ilp64 -lmkl_blacs_intelmpi_ilp64 -liomp5 -lpthread -lm -ldl" 


# determine where this script is located in the file system, and
# assign NWCHEM_TOP based on that
script_dir="$(dirname "$(readlink -f ${BASH_SOURCE[0]})")"
export NWCHEM_TOP=$script_dir

echo "NWCHEM_TOP=$NWCHEM_TOP"
#echo "MPI_LOC=$MPI_LOC"

echo "You might want to change MAX_NPROC in src/tools/global/src/config.h"

echo "You need to do the following to build:"
echo "> cd $NWCHEM_TOP/src"
echo "> make nwchem_config NWCHEM_MODULES=qmandpw"
echo "> make CC=$CC FC=$FC" 

export NWCHEM_EXEC=$NWCHEM_TOP/bin/$NWCHEM_TARGET/nwchem
export NWCHEM_EXECUTABLE=$NWCHEM_TOP/bin/$NWCHEM_TARGET/nwchem
export PATH=$NWCHEM_TOP/bin/$NWCHEM_TARGET:$PATH
#export NWCHEM_MODULES=smallqm
export NWCHEM_MODULES=qmandpw
export NWCHEM_BASIS_LIBRARY=$NWCHEM_TOP/src/basis/libraries/

#export ARMCI_NETWORK=OPENIB
#export MSG_COMMS=MPI
#export IB_INCLUDE=/usr/include/infiniband
#export IB_INCLUDE=/usr/src/kernels/2.6.18-348.6.1.el5-x86_64/include/config/infiniband
#export IB_HOME=/usr
#export IB_LIB=/usr/lib64
#export IB_LIB_NAME="-libumad -libverbs -lp-thread"
#export IB_LIB_NAME="-libumad -libverbs -lpthread"


# set scratch dir
if test -z $PBS_ENVIRONMENT
then
 export SCRATCH_DIR=$PBSTMPDIR
else
 export SCRATCH_DIR=/scratch
fi

#cd $NWCHEM_TOP/src
#make nwchem_config NWCHEM_MODULES=qmandpw
#make CC=$CC FC=$FC
