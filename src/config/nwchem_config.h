# This configuration generated automatically on wonggpu at Fri Jul 5 00:45:45 PDT 2019
# Request modules from user: qmandpw
NW_MODULE_SUBDIRS = NWints atomscf ddscf gradients moints nwdft rimp2 stepper driver optim cphf ccsd vib mcscf esp hessian dplot mp2_grad property solvation nwpw fft vscf etrans bq cons dimqm
NW_MODULE_LIBS = -lccsd -lmcscf -lmp2 -lmoints -lstepper -ldriver -loptim -lnwdft -lgradients -lcphf -lesp -lddscf -lguess -lhessian -lvib -lnwcutil -lrimp2 -lproperty -lsolvation -lnwints -lnwpw -lofpw -lpaw -lpspw -lband -lnwpwlib -lpfft -ldplot -lvscf -letrans -lpspw -lbq -lcons -ldimqm
EXCLUDED_SUBDIRS = develop nwxc scfaux prepar selci qhop rimp2_grad python argos analyz diana nwmd cafe space drdy uccsdt qmmm rism qmd tce geninterface mm perfm dntmc smd nbo dangchang leps ccca lucia rdmft
CONFIG_LIBS = 
