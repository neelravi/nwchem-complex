  

!     Calculate angle between the two MO vectors
      subroutine get_angle_complex_cp2k(
     &              gamma, ! ou: angle to rotate MO pair (s,t)
     &              gamma_max,
     &              g_c,g_uc,
     &              m,n,   ! in: index of MOs to be rotated (only m and n will be rotated) 
     &              nbf,   ! in: nr basis functions
     &              iter)

      implicit none
#include "errquit.fh"
#include "global.fh"
#include "util.fh"
#include "msgtypesf.h"
#include "msgids.fh"
#include "nwc_const.fh"
#include "mafdecls.fh"
      integer u,x,nbf,iter,m,n,g_c,g_uc(4),msg_losc, i, g_conjg_c
      integer g_temp, g_temp1
      complex*16  u11,u22,u12,u21
      parameter(msg_losc=msg_ftn_base-951)
      character*32  pname
      ! gcm and gcn are g_c(mth column) and g_c(nth column)
      complex*16 gcm(nbf), gcn(nbf), gcxm(nbf,3), gcxn(nbf,3)
      double precision gamma, gamma_max , absgamma, A,B, ratio, d2
      pname = "get_angle_complex:"

      A = 0.0d0
      B = 0.0d0

      if (.not. ga_create(mt_dcpl,nbf,nbf,'g_conjg_c',
     $      nbf,nbf,g_conjg_c)) call errquit(pname//'conj',0, GA_ERR)
      call ga_zero(g_conjg_c)

      if (.not. ga_create(mt_dcpl,nbf,nbf,'g_temp',
     $      nbf,nbf,g_temp)) call errquit(pname//'temp',0, GA_ERR)
      call ga_zero(g_temp)


      call ga_copy(g_c, g_temp)
      call ga_conjg(g_temp, g_conjg_c)
      ! remove line below


!     storing the mth column of g_c in gcm and nth column in gcn
      call ga_get(g_conjg_c, 1, nbf, m, m, gcm, nbf)
      call ga_get(g_conjg_c, 1, nbf, n, n, gcn, nbf)      
 

      do x=1,3

       u12=ga_zdot_patch(g_conjg_c,'n',1,nbf,m,m,g_uc(x),'n',1,nbf,n,n)
       u21=ga_zdot_patch(g_conjg_c,'n',1,nbf,n,n,g_uc(x),'n',1,nbf,m,m)
       u11=ga_zdot_patch(g_conjg_c,'n',1,nbf,m,m,g_uc(x),'n',1,nbf,m,m)
       u22=ga_zdot_patch(g_conjg_c,'n',1,nbf,n,n,g_uc(x),'n',1,nbf,n,n)

       
       A = A + dble(  (u12)*conjg(u12) -                                ! cp2k implementation
     &                       0.25d0*(conjg(u11)-conjg(u22))*(u11-u22) )
       B = B + dble( conjg(u12)*(u11 - u22))


C        A = A + dble(u12)*dble(u12) -                                  ! original formula nwchem
C      &                       0.25d0*(dble(u11)-dble(u22))**2
C        B = B + dble(u12)*(dble(u11) - dble(u22))

   
       if (abs(u12-conjg(u21))/max(1.d0,abs(u12)).gt.1d-8) then
        write(6,*) ' U12, U21 ', u12, u21
        call errquit('bad u12', 0, UNKNOWN_ERR)
       endif
      enddo ! end-loop-x

!     check second derivative also
C       d2 = B*SIN(4.0d0*gamma)-A*COS(4.0d0*gamma)
C       IF (d2 <= 0.0d0) THEN ! go to the maximum, not the minimum
C          print*, "ravindra get_angle double derivative", d2
C          IF (gamma > 0.0d0) THEN ! make theta as small as possible
C          print*, "ravindra get_angle d2  less than zero ", gamma
C             gamma = gamma+0.25d0*3.1415926
C          ELSE
C          print*, "ravindra get_angle d2 greater than zero ", gamma
C             gamma = gamma-0.25d0*3.1415926
C          ENDIF
C       ENDIF



!     uncomment everything
      if (ga_nodeid().eq.0) then
        gamma = 0.25d0*ATAN(-B/A)                                      ! cp2k
!        ratio = -A/dsqrt(A*A+B*B)
!        print*, "ratio", ratio
!        gamma = 0.25d0*dacos(ratio)                                     ! nwchem
!        gamma = sign(gamma,B)
        gamma_max = max(gamma_max, abs(gamma))

       if (iter .eq. 1 .and. abs(gamma).lt. 0.01d0) then                ! comment ravindra
       print*, "abs gamma less than zero"
!        gamma = gamma-0.25d0*3.1415926                                 ! cp2k 
!         gamma = (util_random(0)-0.5d0)*3.14d0                          ! nwchem
       endif
      endif

      call ga_brdcst(msg_losc,gamma,
     &               ma_sizeof(mt_dbl,1,mt_byte),0)              
      call ga_brdcst(msg_losc,gamma_max,
     &               ma_sizeof(mt_dbl,1,mt_byte),0)  

        if (.not. ga_destroy(g_temp)) call errquit
     &   (pname//'could not destroy g_temp', 0, GA_ERR)
        if (.not. ga_destroy(g_conjg_c)) call errquit
     &   (pname//'could not destroy g_conjg_c', 0, GA_ERR)

      return 
      end

!     Rotate the pair of orbitals
      subroutine rotate_MOpair_complex_cp2k(
     &                      g_c,   ! in: MO array
     &                      m,     ! in: MO index s
     &                      n,     ! in: MO index t
     &                      nbf,   ! in: nr basis functions
     &                      angle) ! in: angle to rotate [radians]

      
      implicit none
#include "errquit.fh"
#include "mafdecls.fh"
#include "global.fh"
#include "util.fh"
      integer g_c,m,n,nbf,g_rot,g_v,g_w, i,j 
      integer g_cmplx_rot, g_cmplx_v, g_cmplx_w, g_cmplx_c
      logical ga_copy_dz
      external ga_copy_dz
      double precision angle,c,s,rot(2,2)
      complex*16 one_cmplx , zero_cmplx, iota_cmplx
      character*32 pname
      complex*16 vector_m(nbf), vector_n(nbf)

        one_cmplx =dcmplx( 1.0d0,0.0d0 )
        zero_cmplx=dcmplx( 0.0d0,0.0d0 )
        iota_cmplx=dcmplx( 0.0d0,1.0d0 )        


      pname='rotate_MOpair_complex:'
      if (.not. ga_create(mt_dbl,2,2,'g_rot',2,0,g_rot))
     & call errquit(pname//'creating g_rot',0, GA_ERR)

      if (.not. ga_create(mt_dcpl,2,2,'g_cmplx_rot',2,0,g_cmplx_rot))
     & call errquit(pname//'creating g_rot',0, GA_ERR)  

      if (.not. ga_create(mt_dcpl,nbf,2,'g_cmplx_v',nbf,2,g_cmplx_v))
     & call errquit(pname//'creating g_cmplx_v',0, GA_ERR)

      if (.not. ga_create(mt_dcpl,nbf,2,'g_cmplx_w',2,nbf,g_cmplx_w))
     & call errquit(pname//'creating g_cmplx_w',0, GA_ERR)

      call ga_copy_patch('n',g_c,1,nbf,m,m,g_cmplx_v,1,nbf,1,1)
      call ga_copy_patch('n',g_c,1,nbf,n,n,g_cmplx_v,1,nbf,2,2)

      call ga_get(g_cmplx_v, 1, nbf, 1, 1, vector_m, nbf)                      ! temporary. delete afterwards
      call ga_get(g_cmplx_v, 1, nbf, 2, 2, vector_n, nbf)                      ! temporary. delete afterwards     


!      c = cos(angle)
!      s = sin(angle)
!      rot(1,1)= c
!      rot(1,2)= -s
!      rot(2,1)=  s
!      rot(2,2)= c
      
!      call ga_put(g_rot,1,2,1,2,rot,2)

!      if (.not. ga_copy_dz(g_rot, g_cmplx_rot))  call                              ! temporary. unhash afterwards
!     +    errquit('Error : copying double rotation to complex rotation')           ! temporary. unhash afterwards


      CALL zrot(nbf, vector_m, 1, vector_n, 1, cos(angle), sin(angle))

      call ga_put(g_cmplx_v, 1, nbf, 1, 1, vector_m, nbf)                      ! temporary. delete afterwards
      call ga_put(g_cmplx_v, 1, nbf, 2, 2, vector_n, nbf)                      ! temporary. delete afterwards     


!      call ga_zgemm('n','n', nbf, 2, 2, one_cmplx, g_cmplx_v,                      ! temporary. unhash afterwards
!     &     g_cmplx_rot,  zero_cmplx , g_cmplx_w) ! g_w rotated MO pair (m,n)       ! temporary. unhash afterwards

!      call ga_copy_patch('t',g_cmplx_w, 1, nbf, 1, 1, g_c,                        ! temporary. unhash afterwards
!     &                    1,nbf,m,m)                                              ! temporary. unhash afterwards
!      call ga_copy_patch('t',g_cmplx_w, 1, nbf, 2, 2, g_c,                        ! temporary. unhash afterwards
!     &                    1,nbf,n,n)                                              ! temporary. unhash afterwards


      call ga_copy_patch('t',g_cmplx_v, 1, nbf, 1, 1, g_c,                         ! temporary. delete afterwards
     &                    1,nbf,m,m)                                               ! temporary. delete afterwards
      call ga_copy_patch('t',g_cmplx_v, 1, nbf, 2, 2, g_c,                         ! temporary. delete afterwards
     &                    1,nbf,n,n)                                               ! temporary. delete afterwards



c      if (ga_nodeid().eq.0) write(*,*) 'g_c(new):'
c      call ga_print(g_c)
      if (.not. ga_destroy(g_cmplx_v))  call errquit('ga v',0, GA_ERR)
      if (.not. ga_destroy(g_cmplx_w))  call errquit('ga w',0, GA_ERR)
      if (.not. ga_destroy(g_rot))call errquit('ga rot real',0, GA_ERR)
      if (.not. ga_destroy(g_cmplx_rot))call errquit('ga rot',0, GA_ERR)      
      return
      end


!     Purpose : Foster-Boys localization with complex molecular orbitals
!     Author  : Fredy W. Aquino and Ravindra 
!     Date    : March 2019

!     Actual FB subroutine which will maximize <r>.
      subroutine localization_Foster_Boys_complex_RS_cp2k(
     &                     basis, 
     &                     nloc, 
     &                     iloc, 
     &                     nbf, 
     &                     nmo,
     &                     ispin,     
     &                     g_c,                                         
     &                     g_uc,
     &                     rtdb,
     &                     noc_fon,
     &                     focc)
!     Description of parameters
!     basis ::  (in): basis
!     nloc  ::  (in): number of functions to be localized
!     iloc  ::  (in): index of localized function 
!     nbf   ::  (in): number of atomic basis functions
!     nmo   ::  (in): 
!     ispin ::  (in): alpha or beta
!     gc    ::  (in): complex molecular orbitals array
!     uc    ::  (in): complex dipole and overlap array, (x,y,z, overlap)
!     noc_fon :: (in) : array: number of occupied orbitals for each spin.
!     focc  ::   (in) : array : contains the occupied orb info
      implicit none
#include "errquit.fh"
#include "nwc_const.fh"
#include "mafdecls.fh"
#include "global.fh"
#include "geom.fh"
#include "bas.fh"
#include "util.fh"
#include "rtdb.fh" 
c
c     Localize the nloc orbitals in iloc(*) by mixing with each other
c
      integer maxat
      parameter (maxat = nw_max_atom)
      integer basis, nloc, iloc(*), nbf, nmo,ispin, 
     &        g_c, g_uc(4), ! x, y, z, overlap
     &        nrot, set, pair, neven, x, natoms, nlist,
     &        iter, iter_final, ss, s, tt, t, u, geom, a, bflo, bfhi,
     &        list(maxat), rtdb, flag2rdctr , unitno, unitno2, g_temp3,
     &        noc_fon, g_conjg_c, g_temp, i, j, g_occ_vecs, g_temp2
      double precision 
     &        u1, 
     &        gamma, d, tmp, pop(maxat), qas,
     &        qs,dprev, tol, dmax, gamma_tol, gamma_max  
      double precision focc(*)
      complex*16  c(nbf, 2), uc(nbf, 2, 4), rmat(2,noc_fon,noc_fon)
      character*32  pname, spin
      character*500 filename, filename2      
      logical existing
      external ga_prn_dbg0,ga_prn_dbg1,ga_prn_dbg2,
     &         ga_prn_dbg3,ga_prn_dbg4,
     &         get_angle_complex,rotate_MOpair_complex

      pname = "localizeFB_complex_cp2k:"
      if (.not. bas_geom(basis, geom)) 
     &  call errquit(pname//'basis',0,BASIS_ERR)
      if (.not. geom_ncent(geom, natoms)) 
     &  call errquit(pname//'geom',0, GEOM_ERR)
c
      if (natoms.gt.maxat) 
     &  call errquit(pname//'maxat too small ',911,UNKNOWN_ERR)
     

      if (.not. ga_create(mt_dcpl,nbf,nbf,'g_conjg_c',
     $      nbf,-1,g_conjg_c)) call errquit(pname//'conj',0, GA_ERR)
      call ga_zero(g_conjg_c)

      if (.not. ga_create(mt_dcpl,nbf,nbf,'g_temp',
     $      nbf,-1,g_temp)) call errquit(pname//'temp',0, GA_ERR)
      call ga_zero(g_temp)


      call ga_copy(g_c, g_temp)
      call ga_conjg(g_temp, g_conjg_c)

      call ga_zero(g_temp)

      tol = 1d-8
      gamma_tol = 1d-6
c
c      if (ga_nodeid() .eq. 0) then
c         write(6,2)
c 2       format(/10x,' iter   Max. dipole2   Mean dipole2    Converge'/
c     $        10x,' ----   ------------   ------------   ---------')
c         call util_flush(6)
c      end if

      dprev = 0.0d0
      gamma_max = 0.0d0

!     Files required for producing the centroid positions
      unitno  = 180
      unitno2 = 181

      if (ispin == 1) then
          spin = "alpha_"
        else
          spin = "beta_"
      endif

      write(filename, '(2a)') trim(spin),'centroids_FB.xyz'
      write(filename2,'(2a)') trim(spin),'centroids_FB_history.xyz'   

      inquire(file=trim(spin)//"centroids_FB.xyz", exist=existing)
      if (.not. existing) then 
        open(unitno, status ='new',form='formatted',file=trim(filename))
      else
        open(unitno, status ='replace',form='formatted',
     &                                       file=trim(filename))        
      endif
 
      inquire(file=trim(spin)//"centroids_FB_history.xyz", 
     &                                     exist=existing)
      if (existing) then 
       open(unitno2, status='replace', form='formatted',
     &   file=trim(spin)//'centroids_FB_history.xyz', position='append')
         else 
       open(unitno2, status='new',form='formatted', 
     &   file=trim(spin)//'centroids_FB_history.xyz', position='append')
      endif


      write(*,*) " iter       fmax      s    t   nrot    gamma"
      do iter = 1, 20                   ! localization steps

!     create a rmat similar to cp2k rmat which will be an identity matrix first 
!     but the rotations will be applied to it during the localizations. The size 
!     of the matrix will be number of occupied orbitals x number of occupied orbitals

      rmat = (0.0d0,0.0d0)
      ForAll(i = 1:noc_fon) rmat(ispin,i,i) = (1.0d0,0.0d0)

      print*, "ravindra initial rmat"  ! ravindra
      print*, rmat(ispin,:,:)          ! ravindra



c       call ga_sync
       nrot = 0
c     Analyze convergence by forming functional
       d    = 0.0d0
       dmax = 0.0d0
       do ss = 1,nloc
        s = iloc(ss)
        qs = 0.0d0
        do x = 1, 3
         u1=realpart(ga_zdot_patch(g_conjg_c,'n',1,nbf,s,s,
     &                               g_uc(x),'n',1,nbf,s,s))            ! checked and found correct Ravindra

         qs=qs+u1*u1
        enddo ! end-loop-x
        dmax = max(dmax,qs)
        d=d+qs
       enddo ! end-loop-ss
       

#ifdef NWCHEM_USE_GOP_ABSMAX
         call ga_dgop(1, gamma_max, 1, 'absmax')
         call ga_dgop(1, dmax, 1, 'absmax')
#else
         gamma_max = abs(gamma_max)
         dmax      = abs(dmax)
         call ga_dgop(1, gamma_max, 1, 'max')
         call ga_dgop(1, dmax, 1, 'max')
#endif
         call ga_dgop(2, d , 1, '+')


         dprev = d
         if (iter.gt.1 .and. gamma_max.lt.tol) goto 1000
         gamma_max = 0.0d0
c


c     Loop over pairs with as much parallelism as possible
c 
         do s = 1 , noc_fon
           do t = s, noc_fon


         call print_localization_centers_complex(
     &                                geom,  ! in: geom handle
     &                                iter,  ! in: iteration nr
     &                                g_c,   ! in: MO coeffs
     &                                g_uc,  ! in: x,y,z,overlap  
     &                                nloc,  ! in: nr of localizations to do
     &                                iloc,  ! in: states to localize
     &                                nbf,   ! in: nr basis functions
     &                                nmo,   ! in: nr MO coeffs
     &                                natoms, ! in: nr atoms
     &                                unitno2) ! in:unit of file


            
            call get_angle_complex_cp2k(   ! Get angle to rotate pair of MO orbitals (s,t)  
     &                 gamma, ! ou: angle to rotate MO pair (s,t)
     &                 gamma_max,
     &                 g_c,g_uc,
     &                 s,t,
     &                 nbf,   ! in: nr basis functions
     &                 iter) 

 
            if (abs(gamma) .gt. gamma_tol) then ! if-abs(gamma) gt gamma_tol---START
              nrot = nrot + 1
              if (ga_nodeid().eq.0) then
               write(*,'(I2,1X,F15.8,3(2X,I3),4X,F7.2)') 
     &           iter, d, s, t, nrot, gamma*180.0d0/3.14159654
              endif   

!     rotate rmat here
      CALL zrot(noc_fon, rmat(ispin, 1, s), 1, rmat(ispin, 1, t), 1, 
     &                        cos(gamma), cmplx(sin(gamma)))

!      print*, "rotated rmat"
!      do i = 1, noc_fon
!      print*, (rmat(ispin, i,j), j=1,noc_fon)
!      enddo

            end if ! if-abs(gamma) gt gamma_tol---END
          end do ! end-loop-over-pair-t
         end do ! end-loop-over-pair-s

      iter_final = iter

      end do ! end-loop-iter
 1000 continue


!     rotate orbitals once the final rmat is formed.
      if (.not. ga_create(mt_dcpl,nbf,noc_fon,'g_occ_vecs',
     $      nbf,noc_fon,g_occ_vecs)) call errquit('occ_vecs',0, GA_ERR)
      call ga_zero(g_occ_vecs)

      if (.not. ga_create(mt_dcpl, noc_fon, noc_fon, 'g_temp2',
     $     noc_fon, noc_fon, g_temp2)) call errquit('temp2',0, GA_ERR)
      call ga_zero(g_temp2)

      if (.not. ga_create(mt_dcpl, nbf, noc_fon, 'g_temp3',
     $     nbf, noc_fon, g_temp3)) call errquit('temp3',0, GA_ERR)
      call ga_zero(g_temp3)

      print*, "print the occupied patch"
      call ga_print_patch(g_c,1,nbf,1,noc_fon,0)

      ! get the occupied vectors
      call ga_copy_patch("N", g_c, 1, nbf, 1, noc_fon, 
     &                 g_occ_vecs, 1, nbf, 1, noc_fon)


      print*, "compare with the created global array"
      call ga_print(g_occ_vecs)

      ! put the rmat into a global array
      call ga_put(g_temp2, 1, noc_fon, 1, noc_fon, rmat, noc_fon)
      print*, "printing rmat array"
      call ga_print(g_temp2)

      ! multiply vectors and rotation matrix and store in a temp array
      call ga_zgemm("N", "N", nbf, noc_fon, noc_fon, (1.0d0,0.0d0), 
     &                g_occ_vecs, g_temp2, (0.0d0,0.0d0), g_temp3)

      print*, "print the product of vectors and rmat"
      call ga_print(g_temp3)

      ! update the g_c with rotated occupied molecular vectors
      call ga_copy_patch("N", g_temp3, 1, nbf, 1, noc_fon,
     &                            g_c, 1, nbf, 1, noc_fon)

      print*, "print the entire mo global array"
      call ga_print(g_c)


c ====== ADDED by FA-01-26-17 ========== START
        if (.not. rtdb_get(rtdb,'fosic_input:flag2rdctr', 
     &      mt_int, 1, flag2rdctr)) flag2rdctr=0  

        if (ga_nodeid().eq.0) then
         write(*,*) 'In localize_FA:: flag2rdctr=',flag2rdctr
        endif

         if (flag2rdctr.eq.0) then ! flag2rdctr ---- START
!           opening files for writing centroids
      

         call print_localization_centers_complex(
     &                                geom,  ! in: geom handle
     &                                iter_final,! in: iteration nr
     &                                g_c,   ! in: MO coeffs
     &                                g_uc,  ! in: x,y,z,overlap  
     &                                nloc,  ! in: nr of localizations to do
     &                                iloc,  ! in: states to localize
     &                                nbf,   ! in: nr basis functions
     &                                nmo,   ! in: nr MO coeffs
     &                                natoms, ! in: nr atoms
     &                                unitno) ! in:unit of file
         endif ! flag2rdctr ---- END
      close(unitno)
      close(unitno2)

c ====== ADDED by FA-01-26-17 ========== END
c
!     Following portion looks repetative, commented by Ravindra
c     Analyze localization of each mo
c
C       if (ga_nodeid() .eq. 0) then
C          write(6,*)
C          do ss = 1, nloc
C             s = iloc(ss)
C             call ga_get(g_c,  1, nbf, s, s, c(1,1), 1)
C             call ga_get(g_uc(4), 1, nbf, s, s,uc(1,1,1), 1)
C             nlist = 0
C             do a = 1, natoms
C                if (.not. bas_ce2bfr(basis, a, bflo, bfhi))
C      &           call errquit(pname//'localized: basis ',0,BASIS_ERR)
C                qas  = 0.0d0
C                do u = bflo, bfhi
C                   qas  = qas  + c(u,1)*uc(u,1,1)
C                end do
C                if (abs(qas) .gt. 0.01d0) then
C                   nlist = nlist + 1
C                   list(nlist) = a
C                   pop(nlist) = qas
C                end if
C             end do
C             do u = 1, nlist
C                do t = 1, u-1
C                   if (abs(pop(t)).lt.abs(pop(u))) then
C                      tmp = pop(u)
C                      pop(u) = pop(t)
C                      pop(t) = tmp
C                      tt = list(u)
C                      list(u) = list(t)
C                      list(t) = tt
C                   end if
C                end do
C             end do
C             write(6,77) s, (list(a), pop(a), a=1,nlist)
C  77         format(i5, 100(2x,i4,'(',f5.2,')'))
C          end do
C          call util_flush(6)
C       end if
c
c      call ga_sync
c
        if (.not. ga_destroy(g_temp)) call errquit
     &   (pname//'could not destroy g_temp', 0, GA_ERR)
        if (.not. ga_destroy(g_conjg_c)) call errquit
     &   (pname//'could not destroy g_conjg_c', 0, GA_ERR)

      end

