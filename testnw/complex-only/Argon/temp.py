from pyscf import gto, scf
from pyscf.lo.pipek import atomic_pops
mol = gto.M(atom='O 0 0 0; O 0 0 1')
mf = scf.UHF(mol)
mf.verbose=4
mf = scf.addons.dynamic_sz_(mf)
mf.kernel()
print('S^2 = %s, 2S+1 = %s' % mf.spin_square())
fb = atomic_pops(mol, mo_coeff, method='meta_lowdin')