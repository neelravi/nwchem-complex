  

!     Calculate angle between the two MO vectors
      subroutine get_angle_complex(
     &              gamma, ! ou: angle to rotate MO pair (s,t)
     &              gamma_max,
     &              g_c,g_uc,
     &              m,n,   ! in: index of MOs to be rotated (only m and n will be rotated) 
     &              nbf,   ! in: nr basis functions
     &              iter)

      implicit none
#include "errquit.fh"
#include "global.fh"
#include "util.fh"
#include "msgtypesf.h"
#include "msgids.fh"
#include "nwc_const.fh"
#include "mafdecls.fh"
      integer u,x,nbf,iter,m,n,g_c,g_uc(4),msg_losc, i, g_conjg_c
      integer g_temp, g_temp1
      complex*16   u11,u22,u12,u21
      parameter(msg_losc=msg_ftn_base-951)
      character*32  pname
      ! gcm and gcn are g_c(mth column) and g_c(nth column)
      complex*16 gcm(nbf), gcn(nbf), gcxm(nbf,3), gcxn(nbf,3)
      double precision gamma, gamma_max , absgamma, A,B
      pname = "get_angle_complex:"

      A = 0.0d0
      B = 0.0d0

      if (.not. ga_create(mt_dcpl,nbf,nbf,'g_conjg_c',
     $      nbf,-1,g_conjg_c)) call errquit(pname//'conj',0, GA_ERR)
      call ga_zero(g_conjg_c)

      if (.not. ga_create(mt_dcpl,nbf,nbf,'g_temp',
     $      nbf,-1,g_temp)) call errquit(pname//'temp',0, GA_ERR)
      call ga_zero(g_temp)


      call ga_copy(g_c, g_temp)
      call ga_conjg(g_temp, g_conjg_c)
      ! remove line below


!     storing the mth column of g_c in gcm and nth column in gcn
      call ga_get(g_conjg_c, 1, nbf, m, m, gcm, nbf)
      call ga_get(g_conjg_c, 1, nbf, n, n, gcn, nbf)      
 

      do x=1,3
C       call ga_get(g_uc(x), 1, nbf, n, n, gcxn(:,x), nbf)
C       call ga_get(g_uc(x), 1, nbf, m, m, gcxm(:,x), nbf)

C       u12 = ddot(nbf, realpart(gcm), 1, realpart(gcxn(:,x)), 1)
C       u21 = ddot(nbf, realpart(gcn), 1, realpart(gcxm(:,x)), 1)
C       u11 = ddot(nbf, realpart(gcm), 1, realpart(gcxm(:,x)), 1)
C       u22 = ddot(nbf, realpart(gcn), 1, realpart(gcxn(:,x)), 1)
       u12=ga_zdot_patch(g_conjg_c,'n',1,nbf,m,m,g_uc(x),'n',1,nbf,n,n)
       u21=ga_zdot_patch(g_conjg_c,'n',1,nbf,n,n,g_uc(x),'n',1,nbf,m,m)
       u11=ga_zdot_patch(g_conjg_c,'n',1,nbf,m,m,g_uc(x),'n',1,nbf,m,m)
       u22=ga_zdot_patch(g_conjg_c,'n',1,nbf,n,n,g_uc(x),'n',1,nbf,n,n)

       A = A + dble(u12)*dble(u12) - 
     &                       0.25d0*(dble(u11)-dble(u22))**2
       B = B + dble(u12)*(dble(u11) - dble(u22))

!       print '(3I1,3(1X,f15.8),2(1X,f15.8))', x,m,n,u11,u12,u22, A, B


       if (abs(u12-conjg(u21))/max(1.d0,abs(u12)).gt.1d-8) then
        if (ga_nodeid().eq.0) write(6,*) ' U12, U21 ', u12, u21
         call errquit('bad u12', 0, UNKNOWN_ERR)
       endif
      enddo ! end-loop-x
!     The following loop will change for complex numbers 
!     
!     uncomment everything
      if (ga_nodeid().eq.0) then
       gamma = 0.25d0*dacos(-A/dsqrt(A*A+B*B))
       gamma = sign(gamma,B)
       gamma_max = max(gamma_max, abs(gamma))

!       if (iter .eq. 1 .and. abs(gamma).lt. 0.01d0) then
!        gamma = (util_random(0)-0.5d0)*3.14d0
!       endif
      endif
      call ga_brdcst(msg_losc,gamma,
     &               ma_sizeof(mt_dbl,1,mt_byte),0)              
      call ga_brdcst(msg_losc,gamma_max,
     &               ma_sizeof(mt_dbl,1,mt_byte),0)  

        if (.not. ga_destroy(g_temp)) call errquit
     &   (pname//'could not destroy g_temp', 0, GA_ERR)
        if (.not. ga_destroy(g_conjg_c)) call errquit
     &   (pname//'could not destroy g_conjg_c', 0, GA_ERR)

      return 
      end

!     Rotate the pair of orbitals
      subroutine rotate_MOpair_complex(
     &                      g_c,   ! in: MO array
     &                      m,     ! in: MO index s
     &                      n,     ! in: MO index t
     &                      nbf,   ! in: nr basis functions
     &                      angle) ! in: angle to rotate [radians]

      
      implicit none
#include "errquit.fh"
#include "mafdecls.fh"
#include "global.fh"
#include "util.fh"
      integer g_c,m,n,nbf,g_rot,g_v,g_w, i,j 
      integer g_cmplx_rot, g_cmplx_v, g_cmplx_w, g_cmplx_c
      logical ga_copy_dz
      external ga_copy_dz
      double precision angle,c,s,rot(2,2)
      complex*16 one_cmplx , zero_cmplx, iota_cmplx
      character*32 pname

        one_cmplx =dcmplx( 1.0d0,0.0d0)
        zero_cmplx=dcmplx( 0.0d0,0.0d0)
        iota_cmplx=dcmplx( 0.0d0,1.0d0)        


      pname='rotate_MOpair_complex:'
      if (.not. ga_create(mt_dbl,2,2,'g_rot',2,0,g_rot))
     & call errquit(pname//'creating g_rot',0, GA_ERR)

      if (.not. ga_create(mt_dcpl,2,2,'g_cmplx_rot',2,0,g_cmplx_rot))
     & call errquit(pname//'creating g_rot',0, GA_ERR)  

      if (.not. ga_create(mt_dcpl,nbf,2,'g_cmplx_v',nbf,2,g_cmplx_v))
     & call errquit(pname//'creating g_cmplx_v',0, GA_ERR)

      if (.not. ga_create(mt_dcpl,nbf,2,'g_cmplx_w',2,nbf,g_cmplx_w))
     & call errquit(pname//'creating g_cmplx_w',0, GA_ERR)

      call ga_copy_patch('n',g_c,1,nbf,m,m,g_cmplx_v,1,nbf,1,1)
      call ga_copy_patch('n',g_c,1,nbf,n,n,g_cmplx_v,1,nbf,2,2)

      c = cos(angle)
      s = sin(angle)
      rot(1,1)= c
      rot(1,2)= -s
      rot(2,1)=  s
      rot(2,2)= c
      
      call ga_put(g_rot,1,2,1,2,rot,2)

      if (.not. ga_copy_dz(g_rot, g_cmplx_rot))  call 
     +    errquit('Error : copying double rotation to complex rotation')


      call ga_zgemm('n','n', nbf, 2, 2, one_cmplx, g_cmplx_v,
     &          g_cmplx_rot, zero_cmplx , g_cmplx_w) ! g_w rotated MO pair (m,n)
c      if (ga_nodeid().eq.0) write(*,*) 'g_w:'
c      call ga_print(g_w)
c      if (ga_nodeid().eq.0) write(*,*) 'g_c(old):'
c      call ga_print(g_c)
      call ga_copy_patch('t',g_cmplx_w, 1, nbf, 1, 1, g_c, 
     &                    1,nbf,m,m)
      call ga_copy_patch('t',g_cmplx_w, 1, nbf, 2, 2, g_c, 
     &                    1,nbf,n,n)
c      if (ga_nodeid().eq.0) write(*,*) 'g_c(new):'
c      call ga_print(g_c)
      if (.not. ga_destroy(g_cmplx_v))  call errquit('ga v',0, GA_ERR)
      if (.not. ga_destroy(g_cmplx_w))  call errquit('ga w',0, GA_ERR)
      if (.not. ga_destroy(g_rot))call errquit('ga rot real',0, GA_ERR)
      if (.not. ga_destroy(g_cmplx_rot))call errquit('ga rot',0, GA_ERR)      
      return
      end


!     Purpose : Foster-Boys localization with complex molecular orbitals
!     Author  : Fredy W. Aquino and Ravindra Shinde
!     Date    : March 2019

!     Actual FB subroutine which will maximize <r>.
      subroutine localization_Foster_Boys_complex_RS(
     &                     basis, 
     &                     nloc, 
     &                     iloc, 
     &                     nbf, 
     &                     nmo,
     &                     ispin,     
     &                     g_c,                                         
     &                     g_uc,
     &                     rtdb,
     &                     noc_fon,
     &                     focc)
!     Description of parameters
!     basis ::  (in): basis
!     nloc  ::  (in): number of functions to be localized
!     iloc  ::  (in): index of localized function 
!     nbf   ::  (in): number of atomic basis functions
!     nmo   ::  (in): 
!     ispin ::  (in): alpha or beta
!     gc    ::  (in): complex molecular orbitals array
!     uc    ::  (in): complex dipole and overlap array, (x,y,z, overlap)
!     noc_fon :: (in) : array: number of occupied orbitals for each spin.
!     focc  ::   (in) : array : contains the occupied orb info
      implicit none
#include "errquit.fh"
#include "nwc_const.fh"
#include "mafdecls.fh"
#include "global.fh"
#include "geom.fh"
#include "bas.fh"
#include "util.fh"
#include "rtdb.fh" 
c
c     Localize the nloc orbitals in iloc(*) by mixing with each other
c
      integer maxat
      parameter (maxat = nw_max_atom)
      integer basis, nloc, iloc(*), nbf, nmo,ispin, 
     &        g_c, g_uc(4), ! x, y, z, overlap
     &        nrot, set, pair, neven, x, natoms, nlist,
     &        iter, iter_final, ss, s, tt, t, u, geom, a, bflo, bfhi,
     &        list(maxat), rtdb, flag2rdctr , unitno, unitno2,
     &        noc_fon
      double precision 
     &        u1, 
     &        gamma, d, tmp, pop(maxat), qas,
     &        qs,dprev, tol, dmax, gamma_tol, gamma_max  
      double precision focc(*)
      complex*16  c(nbf, 2), uc(nbf, 2, 4)
      character*32  pname, spin
      character*500 filename, filename2      
      logical existing
      external ga_prn_dbg0,ga_prn_dbg1,ga_prn_dbg2,
     &         ga_prn_dbg3,ga_prn_dbg4,
     &         get_angle_complex,rotate_MOpair_complex

      pname = "localizeFB_complex:"
      if (.not. bas_geom(basis, geom)) 
     &  call errquit(pname//'basis',0,BASIS_ERR)
      if (.not. geom_ncent(geom, natoms)) 
     &  call errquit(pname//'geom',0, GEOM_ERR)
c
      if (natoms.gt.maxat) 
     &  call errquit(pname//'maxat too small ',911,UNKNOWN_ERR)
     

      tol = 1d-8
      gamma_tol = 1d-10
c
c      if (ga_nodeid() .eq. 0) then
c         write(6,2)
c 2       format(/10x,' iter   Max. dipole2   Mean dipole2    Converge'/
c     $        10x,' ----   ------------   ------------   ---------')
c         call util_flush(6)
c      end if

      dprev = 0.0d0
      gamma_max = 0.0d0

!     Files required for producing the centroid positions
      if (ga_nodeid().eq.0) then 
        unitno  = 180
        unitno2 = 181

        if (ispin == 1) then
            spin = "alpha_"
          else
            spin = "beta_"
        endif

        write(filename, '(2a)') trim(spin),'centroids_FB.xyz'
        write(filename2,'(2a)') trim(spin),'centroids_FB_history.xyz'   

        inquire(file=trim(spin)//"centroids_FB.xyz", exist=existing)
        if (.not. existing) then 
          open(unitno, status ='new',form='formatted',
     &                                         file=trim(filename))
        else
          open(unitno, status ='replace',form='formatted',
     &                                         file=trim(filename))        
        endif
   
        inquire(file=trim(spin)//"centroids_FB_history.xyz", 
     &                                     exist=existing)
        if (existing) then 
         open(unitno2, status='replace', form='formatted',
     &   file=trim(spin)//'centroids_FB_history.xyz', position='append')
           else 
         open(unitno2, status='new',form='formatted', 
     &   file=trim(spin)//'centroids_FB_history.xyz', position='append')
        endif
      endif ! file writing by master node

      if (ga_nodeid().eq.0) write(*,*) 
     &      " iter       fmax      s    t   nrot    gamma"
      
      do iter = 1, 100                     ! localization steps
c       call ga_sync
       nrot = 0
c     Analyze convergence by forming functional
       d    = 0.0d0
       dmax = 0.0d0
       do ss = 1,nloc
        s = iloc(ss)
        qs = 0.0d0
        do x = 1, 3
         u1=ga_zdot_patch(g_c,'n',1,nbf,s,s,g_uc(x),'n',1,nbf,s,s)
         qs=qs+dble(u1)**2
        enddo ! end-loop-x
        dmax = max(dmax,qs)
        d=d+qs
       enddo ! end-loop-ss
       

#ifdef NWCHEM_USE_GOP_ABSMAX
         call ga_dgop(1, gamma_max, 1, 'absmax')
         call ga_dgop(1, dmax, 1, 'absmax')
#else
         gamma_max = abs(gamma_max)
         dmax      = abs(dmax)
         call ga_dgop(1, gamma_max, 1, 'max')
         call ga_dgop(1, dmax, 1, 'max')
#endif
         call ga_dgop(2, d , 1, '+')


         dprev = d
         if (iter.gt.1 .and. gamma_max.lt.tol) goto 1000
         gamma_max = 0.0d0
c
c     Loop over pairs with as much parallelism as possible
c

         neven = nloc + mod(nloc,2)
         do set = 1, neven-1
c          do pair = 1+ga_nodeid(), neven/2, ga_nnodes()
          do pair = 1, neven/2
           call localize_pairs(neven, set, pair, ss, tt)
           if (tt .le. nloc) then ! if-tt le nloc --- START  
            s = iloc(ss)
            t = iloc(tt)
            call get_angle_complex(   ! Get angle to rotate pair of MO orbitals (s,t)  
     &                 gamma, ! ou: angle to rotate MO pair (s,t)
     &                 gamma_max,
     &                 g_c,g_uc,
     &                 s,t,
     &                 nbf,   ! in: nr basis functions
     &                 iter) 

 
            if (abs(gamma) .gt. gamma_tol) then ! if-abs(gamma) gt gamma_tol---START
              nrot = nrot + 1
              if (ga_nodeid().eq.0) then
               write(*,'(I2,1X,F15.8,3(2X,I3),4X,F7.2)') 
     &           iter, d, s, t, nrot, gamma*180.0d0/3.14159654
              endif   

              call rotate_MOpair_complex(
     &                 g_c,   ! io: MO array
     &                 s,     ! in: MO index s
     &                 t,     ! in: MO index t
     &                 nbf,   ! in: nr basis functions
     &                 gamma) ! in: angle to rotate [radians]   
             do x = 1, 4
              call rotate_MOpair_complex(
     &                 g_uc(x),! io: MO array
     &                 s,      ! in: MO index s
     &                 t,      ! in: MO index t
     &                 nbf,    ! in: nr basis functions
     &                 gamma)  ! in: angle to rotate [radians]   
             end do ! end-loop-x

            end if ! if-abs(gamma) gt gamma_tol---END
           end if ! if-tt le nloc --- END
          end do ! end-loop-pair
c          call ga_sync
         end do ! end-loop-set
!     Print the centroids history to a file
         call print_localization_centers_complex(
     &                                geom,  ! in: geom handle
     &                                iter,  ! in: iteration nr
     &                                g_c,   ! in: MO coeffs
     &                                g_uc,  ! in: x,y,z,overlap  
     &                                nloc,  ! in: nr of localizations to do
     &                                iloc,  ! in: states to localize
     &                                nbf,   ! in: nr basis functions
     &                                nmo,   ! in: nr MO coeffs
     &                                natoms, ! in: nr atoms
     &                                unitno2) ! in:unit of file

      iter_final = iter

      end do ! end-loop-iter
 1000 continue


c ====== ADDED by FA-01-26-17 ========== START
        if (.not. rtdb_get(rtdb,'fosic_input:flag2rdctr', 
     &      mt_int, 1, flag2rdctr)) flag2rdctr=0  

        if (ga_nodeid().eq.0) then
         write(*,*) 'In localize_FA:: flag2rdctr=',flag2rdctr
        endif

         if (flag2rdctr.eq.0) then ! flag2rdctr ---- START
!           opening files for writing centroids
      

         call print_localization_centers_complex(
     &                                geom,  ! in: geom handle
     &                                iter_final,! in: iteration nr
     &                                g_c,   ! in: MO coeffs
     &                                g_uc,  ! in: x,y,z,overlap  
     &                                nloc,  ! in: nr of localizations to do
     &                                iloc,  ! in: states to localize
     &                                nbf,   ! in: nr basis functions
     &                                nmo,   ! in: nr MO coeffs
     &                                natoms, ! in: nr atoms
     &                                unitno) ! in:unit of file
         endif ! flag2rdctr ---- END
      close(unitno)
      close(unitno2)
      end

