      subroutine get_angle(
     &              gamma, ! ou: angle to rotate MO pair (s,t)
     &              gamma_max,
     &              g_c,g_uc,
     &              m,n,   ! in: MOs:m,n to rotate
     &              nbf,   ! in: nr basis functions
     &              iter)
      implicit none
#include "errquit.fh"
#include "global.fh"
#include "util.fh"
#include "msgtypesf.h"
#include "msgids.fh"
#include "nwc_const.fh"
#include "mafdecls.fh"
      integer u,x,nbf,iter,m,n,g_c,g_uc(4),msg_losc
      double precision 
     &        A,B,u11,u22,u12,u21,gamma,gamma_max 
      parameter(msg_losc=msg_ftn_base-951)
      character*32  pname
      pname = "get_angle:"
      A = 0.0d0
      B = 0.0d0
      do x=1,3
       u12=ga_ddot_patch(g_c,'n',1,nbf,m,m,g_uc(x),'n',1,nbf,n,n)
       u21=ga_ddot_patch(g_c,'n',1,nbf,n,n,g_uc(x),'n',1,nbf,m,m)
       u11=ga_ddot_patch(g_c,'n',1,nbf,m,m,g_uc(x),'n',1,nbf,m,m)
       u22=ga_ddot_patch(g_c,'n',1,nbf,n,n,g_uc(x),'n',1,nbf,n,n)
       A = A + u12*u12 - 0.25d0*(u11-u22)**2
       B = B + u12*(u11 - u22)
       if (abs(u12-u21)/max(1.0d0,abs(u12)).gt.1d-8) then
        write(6,*) ' U12, U21 ', u12, u21
        call errquit('bad u12', 0, UNKNOWN_ERR)
       endif
      enddo ! end-loop-x
      if (ga_nodeid().eq.0) then
       gamma = 0.25d0*acos(-A/sqrt(A**2+B**2))
       gamma = sign(gamma,B)
       gamma_max = max(gamma_max, abs(gamma))
       if (iter .eq. 1 .and. abs(gamma).lt. 0.01d0) then
        gamma = (util_random(0)-0.5d0)*3.14d0
       endif
      endif
      call ga_brdcst(msg_losc,gamma,
     &               ma_sizeof(mt_dbl,1,mt_byte),0)              
      call ga_brdcst(msg_losc,gamma_max,
     &               ma_sizeof(mt_dbl,1,mt_byte),0)  
      return 
      end

      subroutine rotate_MOpair(
     &                      g_c,   ! in: MO array
     &                      m,     ! in: MO index s
     &                      n,     ! in: MO index t
     &                      nbf,   ! in: nr basis functions
     &                      angle) ! in: angle to rotate [radians]
      implicit none
#include "errquit.fh"
#include "mafdecls.fh"
#include "global.fh"
#include "util.fh"
      integer g_c,m,n,nbf,g_rot,g_v,g_w
      double precision angle,c,s,rot(2,2)
      character*32 pname

      pname='rotate_MOpair:'
      if (.not. ga_create(mt_dbl,2,2,'g_rot',2,0,g_rot))
     & call errquit(pname//'creating g_rot',0, GA_ERR)
      if (.not. ga_create(mt_dbl,nbf,2,'g_v',nbf,2,g_v))
     & call errquit(pname//'creating g_v',0, GA_ERR)
      if (.not. ga_create(mt_dbl,2,nbf,'g_w',2,nbf,g_w))
     & call errquit(pname//'creating g_w',0, GA_ERR)

      call ga_copy_patch('n',g_c,1,nbf,m,m,g_v,1,nbf,1,1)
      call ga_copy_patch('n',g_c,1,nbf,n,n,g_v,1,nbf,2,2)
      c = cos(angle)
      s = sin(angle)
      rot(1,1)= c
      rot(1,2)= s
      rot(2,1)=-s
      rot(2,2)= c
      call ga_put(g_rot,1,2,1,2,rot,2)
c      if (ga_nodeid().eq.0) write(*,*) 'g_v:'
c      call ga_print(g_v)
c      if (ga_nodeid().eq.0) write(*,*) 'g_rot:'
c      call ga_print(g_rot)
      call ga_dgemm('n','t',2,nbf,2,1.0d0,g_rot,g_v,0.0d0,g_w) ! g_w rotated MO pair (m,n)
c      if (ga_nodeid().eq.0) write(*,*) 'g_w:'
c      call ga_print(g_w)
c      if (ga_nodeid().eq.0) write(*,*) 'g_c(old):'
c      call ga_print(g_c)
      call ga_copy_patch('t',g_w,1,1,1,nbf,g_c,1,nbf,m,m)
      call ga_copy_patch('t',g_w,2,2,1,nbf,g_c,1,nbf,n,n)
c      if (ga_nodeid().eq.0) write(*,*) 'g_c(new):'
c      call ga_print(g_c)
      if (.not. ga_destroy(g_v))  call errquit('ga?',0, GA_ERR)
      if (.not. ga_destroy(g_w))  call errquit('ga?',0, GA_ERR)
      if (.not. ga_destroy(g_rot))call errquit('ga?',0, GA_ERR)
      return
      end

      subroutine localizeFB_FA_nw(
     &                     basis, c, uc, 
     &                     nloc, iloc, nbf, nmo,
     &                     g_c, g_uc,
     &                     rtdb)
c Purpose : Foster-Boys localization 
c Author  : Fredy W. Aquino
c Date    : 11-06-18
c Note .- Adapted from localizeFB, increasing parallelization
c         by using global arrays (GAs)
      implicit none
#include "errquit.fh"
#include "nwc_const.fh"
#include "mafdecls.fh"
#include "global.fh"
#include "geom.fh"
#include "bas.fh"
#include "util.fh"
#include "rtdb.fh" ! FA-01-24-18 (to read input keywords)
c
c     Localize the nloc orbitals in iloc(*) by mixing with each other
c
      integer maxat
      parameter (maxat = nw_max_atom)
      integer basis, nloc, iloc(*), nbf, nmo,
     &        g_c, g_uc(4), ! x, y, z, overlap
     &        nrot, set, pair, neven, x, natoms, nlist,
     &        iter, ss, s, tt, t, u, geom, a, bflo, bfhi,
     &        list(maxat), rtdb, flag2rdctr   
      double precision 
     &        c(nbf, 2), uc(nbf, 2, 4), u1, 
     &        gamma, d, tmp, pop(maxat), qas,
     &        qs,dprev, tol, dmax, gamma_tol, gamma_max       
      character*32  pname
      external ga_prn_dbg0,ga_prn_dbg1,ga_prn_dbg2,
     &         ga_prn_dbg3,ga_prn_dbg4,
     &         get_angle,rotate_MOpair

      pname = "localizeFB_FA_nw:"
      if (.not. bas_geom(basis, geom)) 
     &  call errquit(pname//'basis',0,BASIS_ERR)
      if (.not. geom_ncent(geom, natoms)) 
     &  call errquit(pname//'geom',0, GEOM_ERR)
c
      if (natoms.gt.maxat) 
     &  call errquit(pname//'maxat too small ',911,UNKNOWN_ERR)
     
      tol = 1d-8
      gamma_tol = 1d-10
c
c      if (ga_nodeid() .eq. 0) then
c         write(6,2)
c 2       format(/10x,' iter   Max. dipole2   Mean dipole2    Converge'/
c     $        10x,' ----   ------------   ------------   ---------')
c         call util_flush(6)
c      end if
c
      dprev = 0.0d0
      gamma_max = 0.0d0
      do iter = 1, 100
c       call ga_sync
       nrot = 0
c     Analyze convergence by forming functional
       d    = 0.0d0
       dmax = 0.0d0
       do ss = 1,nloc
        s = iloc(ss)
        qs = 0.0d0
        do x = 1, 3
         u1=ga_ddot_patch(g_c,'n',1,nbf,s,s,g_uc(x),'n',1,nbf,s,s)
         qs=qs+u1**2
        enddo ! end-loop-x
        dmax = max(dmax,qs)
        d=d+qs
c        write(*,'(A,3(1X,I3),3(1X,F15.8))') 
c     &      '(iter,ss,s,qs,d,dmax)=',iter,ss,s,qs,d,dmax
       enddo ! end-loop-ss

#ifdef NWCHEM_USE_GOP_ABSMAX
         call ga_dgop(1, gamma_max, 1, 'absmax')
         call ga_dgop(1, dmax, 1, 'absmax')
#else
         gamma_max = abs(gamma_max)
         dmax      = abs(dmax)
         call ga_dgop(1, gamma_max, 1, 'max')
         call ga_dgop(1, dmax, 1, 'max')
#endif
         call ga_dgop(2, d , 1, '+')

c        if (ga_nodeid() .eq. 0) then
c          write(*,'(A,1X,I3,6(1X,F15.8))') 
c     &      '(iter,dmax,d/n,d0/n,gamma_max,(d-d0)/n,tol)=',
c     &       iter, dmax, d/dble(nloc), dprev/dble(nloc), 
c     &       gamma_max,
c     &       abs(d-dprev)/dble(nloc),tol

c          write(6,1) iter, dmax, d/dble(nloc), gamma_max
c 1        format(10x, i5, 2f17.8, 1p,2d12.2)
c          call util_flush(6)
c        endif

c         call ga_sync

         dprev = d
         if (iter.gt.1 .and. gamma_max.lt.tol) goto 1000
         gamma_max = 0.0d0
c
c     Loop over pairs with as much parallelism as possible
c
         neven = nloc + mod(nloc,2)
         do set = 1, neven-1
c          do pair = 1+ga_nodeid(), neven/2, ga_nnodes()
          do pair = 1, neven/2
           call localize_pairs(neven, set, pair, ss, tt)
           if (tt .le. nloc) then ! if-tt le nloc --- START  
            s = iloc(ss)
            t = iloc(tt)
            call get_angle(   ! Get angle to rotate pair of MO orbitals (s,t)  
     &                 gamma, ! ou: angle to rotate MO pair (s,t)
     &                 gamma_max,
     &                 g_c,g_uc,
     &                 s,t,
     &                 nbf,   ! in: nr basis functions
     &                 iter) 
     
            if (abs(gamma) .gt. gamma_tol) then ! if-abs(gamma) gt gamma_tol---START
              nrot = nrot + 1
              if (ga_nodeid().eq.0) then
               write(*,'(2A,10(1X,I3),1X,F15.8,1X,F7.2,1X,D10.3)') 
     &          '(iter,nloc,neven,set,pair,ss,tt,s,t,nrot,',
     &          'gamma,gamma_deg,gamma_tol)=',
     &           iter,nloc,neven,set,pair,ss,tt,s,t,nrot,
     &           gamma,gamma*180.0d0/3.14159654,gamma_tol
              endif   

              call rotate_MOpair(
     &                 g_c,   ! io: MO array
     &                 s,     ! in: MO index s
     &                 t,     ! in: MO index t
     &                 nbf,   ! in: nr basis functions
     &                 gamma) ! in: angle to rotate [radians]   
             do x = 1, 4
              call rotate_MOpair(
     &                 g_uc(x),! io: MO array
     &                 s,      ! in: MO index s
     &                 t,      ! in: MO index t
     &                 nbf,    ! in: nr basis functions
     &                 gamma)  ! in: angle to rotate [radians]   
             end do ! end-loop-x

            end if ! if-abs(gamma) gt gamma_tol---END
           end if ! if-tt le nloc --- END
          end do ! end-loop-pair
c          call ga_sync
         end do ! end-loop-set
      end do ! end-loop-iter
c
 1000 continue

c ====== ADDED by FA-01-26-17 ========== START
        if (.not. rtdb_get(rtdb,'fosic_input:flag2rdctr', 
     &      mt_int, 1, flag2rdctr)) flag2rdctr=0  

        if (ga_nodeid().eq.0) then
         write(*,*) 'In localize_FA:: flag2rdctr=',flag2rdctr
        endif

         if (flag2rdctr.eq.0) then ! flag2rdctr ---- START
         call print_localization_centers(
     &                                geom,  ! in: geom handle
     &                                iter,  ! in: iteration nr
     &                                c,     ! in: scratch arr for g_c
     &                                uc,    ! in: scratch arr for g_uc
     &                                nloc,  ! in: nr of localizations to do
     &                                iloc,  ! in: states to localize
     &                                nbf,   ! in: nr basis functions
     &                                nmo,   ! in: nr MO coeffs
     &                                natoms,! in: nr atoms
     &                                g_c,   ! in: MO coeffs
     &                                g_uc)  ! in: x,y,z,overlap  
         endif ! flag2rdctr ---- END
c ====== ADDED by FA-01-26-17 ========== END
c
c     Analyze localization of each mo
c
      if (ga_nodeid() .eq. 0) then
         write(6,*)
         do ss = 1, nloc
            s = iloc(ss)
            call ga_get(g_c,  1, nbf, s, s, c(1,1), 1)
            call ga_get(g_uc(4), 1, nbf, s, s,uc(1,1,1), 1)
            nlist = 0
            do a = 1, natoms
               if (.not. bas_ce2bfr(basis, a, bflo, bfhi))
     &           call errquit(pname//'localized: basis ',0,BASIS_ERR)
               qas  = 0.0d0
               do u = bflo, bfhi
                  qas  = qas  + c(u,1)*uc(u,1,1)
               end do
               if (abs(qas) .gt. 0.01d0) then
                  nlist = nlist + 1
                  list(nlist) = a
                  pop(nlist) = qas
               end if
            end do
            do u = 1, nlist
               do t = 1, u-1
                  if (abs(pop(t)).lt.abs(pop(u))) then
                     tmp = pop(u)
                     pop(u) = pop(t)
                     pop(t) = tmp
                     tt = list(u)
                     list(u) = list(t)
                     list(t) = tt
                  end if
               end do
            end do
            write(6,77) s, (list(a), pop(a), a=1,nlist)
 77         format(i5, 100(2x,i4,'(',f5.2,')'))
         end do
         call util_flush(6)
      end if
c
c      call ga_sync
c
      end

